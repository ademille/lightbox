from lights import pi_gpio
on_rpi = True
try:
    import RPi.GPIO as GPIO
except ImportError:
    import readchar
    on_rpi = False


class Light_list(object):

    def __init__(self):
        # Setup GPIOs for the Lights
        light_GPIO_list = [4, 17, 27, 22, 18, 23, 24, 25]

        # The list of lights
        self._light_list = []
        first = True
        for pin in light_GPIO_list:
            if on_rpi:
                GPIO.setup(pin, GPIO.OUT)
            pi_GPIO = pi_gpio.Pi_GPIO(pin, first)
            self._light_list += [pi_GPIO]
            first = False

    def get_list(self):
        return self._light_list

import time
from lights import light_pattern
on_rpi = True
try:
    import RPi.GPIO as GPIO
except ImportError:
    import readchar
    on_rpi = False


class Rotate_sequence(object):

    def __init__(self, clockwise, GPIO_list):
        self._clockwise = clockwise
        self._GPIO_list = GPIO_list

    def setup(self):
        """ Setup the initial state of the lights."""

        # Just turn the first two on
        if len(self._GPIO_list) >= 2:
            self._GPIO_list[0].on = True
            self._GPIO_list[1].on = True

        # Turn the rest off
        for i in range(2, len(self._GPIO_list)):
            self._GPIO_list[i].on = False

    def update(self, speed):
        """ Update the new state of the GPIO list"""
        if self._clockwise:
            light_pattern.rotate_cw(self._GPIO_list)
        else:
            light_pattern.rotate_ccw(self._GPIO_list)
        time.sleep(.1 / speed)

        # Apply updated light values
        # TODO: Abstract this
        if on_rpi:
            for light in self._GPIO_list:
                GPIO.output(light.pin, light.on)

        # The sequence should continue
        return True

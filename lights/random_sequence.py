import random
import time
on_rpi = True
try:
    import RPi.GPIO as GPIO
except ImportError:
    import readchar
    on_rpi = False
from lights import light_pattern

class Random_sequence(object):
    ITERATIONS = 10

    def __init__(self, GPIO_list):
        self._max_updates = 0
        self._update_count = 0
        self._rand = random.Random()
        self._rand.seed()
        self._choices_list = []
        self._GPIO_list = GPIO_list

    def setup(self):
        """ Setup the initial state of the lights."""

        # Just turn the first two on
        if len(self._choices_list) == 0:
            print("Starting new list")
            self._choices_list = list(range(len(self._GPIO_list)))
        # Figure out what the final light will be. The list will be truncated
        # if this isn't the first run. This is used to ensure all the choices are selected
        # once before cycling
        list_pos = self._rand.randrange(len(self._choices_list))
        # Grab the light from the remaining choices
        final_light = self._choices_list[list_pos]

        # Remove the final light from the list so that it won't be chosen again
        self._choices_list.remove(final_light)

        self._max_updates = len(self._GPIO_list) * Random_sequence.ITERATIONS + final_light
        self._update_count = 0

        # Just turn the first two on
        if len(self._GPIO_list) >= 2:
            self._GPIO_list[0].on = True
            self._GPIO_list[1].on = True

        # Turn the rest off
        for i in range(2, len(self._GPIO_list)):
            self._GPIO_list[i].on = False

        # Start in normal mode so that the spinning will stop automatically
        self._keep_spinning = False

    def set_keep_spinning(self, enable):
        self._keep_spinning = enable

    def update(self, speed):
        """ Update the new state of the GPIO list"""

        retval = -1 # Continue
        light_pattern.rotate_cw(self._GPIO_list)

        # Sleep based on the passed in speed and also the current update.  Slow
        # down with more iterations
        #print ("Random_sequence[update]: speed = %d, self.max_updated = %d, self._update_count = %d, Random_sequence.ITERATIONS = %d" % (speed, self._max_updates, self._update_count, Random_sequence.ITERATIONS))
        # Basically use the formula y = 1/x. It causes the spinning to be fast
        # at first and then quickly slow down at the end.
        sleep_time = 1 / (10*(speed + (self._max_updates - self._update_count)
                            / Random_sequence.ITERATIONS))
        time.sleep(sleep_time)

        # Keep track of the current update
        self._update_count += 1

        # If this is the last iteration then only leave a single light on
        if self._update_count >= self._max_updates:
            #Check for special case where we stopped on the last element. In this case
            #one light will be at 0 and one will be at the last element in the list.
            if ((len(self._GPIO_list) > 1) and (self._GPIO_list[0].on) and
                    (self._GPIO_list[len(self._GPIO_list) - 1].on)):
                # Skip element zero, because the last element should be turned off
                self._GPIO_list[len(self._GPIO_list) - 1].on = False
            else:
                for i in range(len(self._GPIO_list)):
                    if self._GPIO_list[i].on:
                        self._GPIO_list[i].on = False
                        break

            # Find the light that is on
            for i in range(len(self._GPIO_list)):
                if self._GPIO_list[i].on:
                    retval = (8-i) # Sequence shouldn't continue
        # Apply updated light values
        # TODO: This needs to be abstracted.
        if on_rpi:
            for light in self._GPIO_list:
                GPIO.output(light.pin, light.on)
        return retval

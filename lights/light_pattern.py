
def rotate_cw(gpio_list):
    """Rotate a list of GPIO elements clockwise."""

    size = len(gpio_list)
    if (size > 0):
        # Grab the first element
        first = gpio_list[0].on
        for i in range(size - 1):
            gpio_list[i].on = gpio_list[i + 1].on
        gpio_list[size - 1].on = first


def rotate_ccw(gpio_list):
    """Rotate a list of GPIO elements counter-clockwise."""
    size = len(gpio_list)
    if (size > 0):
        # Grab the last element
        last = gpio_list[size - 1].on
        for i in reversed(range(size)):
            gpio_list[i].on = gpio_list[i-1].on

        gpio_list[0].on = last


def invert(gpio_list):
    """Invert a list of GPIO elements."""
    for gpio in gpio_list:
        gpio.on = not gpio.on

def print_list(gpio_list):
    print ("["),
    for GPIO in gpio_list:
        print("%s, " % GPIO.on),

    print("]")

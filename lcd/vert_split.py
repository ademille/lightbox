def split_text(text, num_cols):
    """ Split a string into optimal word boundaries that fit in num_cols"""
    split_pos = 0
    if len(text) <= num_cols:
        return [text]
    else:
        # Split the string
        str_list = text.split()
        split_list = []
        line = ""
        for s in str_list:
            if len (line) > 0 and (len(line) + len(s) < num_cols):
                # A word has already been added and a space will be appended
                line += " " + s
            else:
                if len(line) > 0:
                    # There is content, so add it
                    split_list.append(line)
                # Create a new line and add the current string
                line = s
        if len(line) > 0:
            split_list.append(line)


        return split_list

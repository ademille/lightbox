from time import sleep
import unittest
from lcd_display import LCD_display


class Test_LCD(unittest.TestCase):
    def setUp(self):
        serial_file = '/dev/ttyACM0'
        try:
            serial_dev = open(serial_file, 'wb')
        except IOError:
            print "Could not open file: %s" % serial_file

        self.lcd = LCD_display(serial_dev)
        self.lcd.clear_screen()

    def tearDown(self):
        self.lcd.shutdown()

    def test_write_text(self):
        self.assertTrue(self.lcd.clear_screen())
        self.assertTrue(self.lcd.write_text("LCD Display Test"))

    def test_set_backlight_enable(self):
        self.assertTrue(self.lcd.set_backlight_enable(False))
        self.assertTrue(self.lcd.set_backlight_enable(True))

    def test_set_brightness(self):
        for i in range(0, 100, 10):
            sleep(.1)
            self.assertTrue(self.lcd.set_brightness(i))

    def test_set_contrast(self):
        for i in range(0, 200, 10):
            sleep(.1)
            self.assertTrue(self.lcd.set_contrast(i))

    def test_set_auto_scroll_enable(self):
        self.assertTrue(self.lcd.set_auto_scroll_enable(False))
        self.assertTrue(self.lcd.set_auto_scroll_enable(True))

    def test_clear_screen(self):
        self.assertTrue(self.lcd.clear_screen())

    def test_set_cursor_pos(self):
        self.assertTrue(self.lcd.set_cursor_pos(2, 10))

    def test_go_home(self):
        self.assertTrue(self.lcd.go_home())

    def test_cursor_back(self):
        self.assertTrue(self.lcd.cursor_back())

    def test_cursor_forward(self):
        self.assertTrue(self.lcd.cursor_forward())

    def test_set_underline_cursor_enable(self):
        self.assertTrue(self.lcd.set_underline_cursor_enable(False))
        self.assertTrue(self.lcd.set_underline_cursor_enable(True))

    def test_set_block_cursor_enable(self):
        self.assertTrue(self.lcd.set_block_cursor_enable(False))
        self.assertTrue(self.lcd.set_block_cursor_enable(True))

    def test_set_background_color(self):
        self.assertTrue(self.lcd.set_background_color(255, 0, 0))
        sleep(.5)
        self.assertTrue(self.lcd.set_background_color(0, 255, 0))
        sleep(.5)
        self.assertTrue(self.lcd.set_background_color(0, 0, 255))
        sleep(.5)
        self.assertTrue(self.lcd.set_background_color(255, 255, 255))
        sleep(.5)

    def test_write_text(self):
        for i in range(0, 100):
            self.assertTrue(self.lcd.write_text("%d " % i))

if __name__ == '__main__':
    unittest.main()

import unittest
from vert_split import split_text


class Test_vert_split(unittest.TestCase):

    def test_single_word_fits(self):
        str_list = split_text("Hello", 5)
        self.assertEqual(str_list, ['Hello'])

    def test_single_word_no_fit(self):
        str_list = split_text("Hello", 4)
        self.assertEqual(str_list, ['Hello'])

    def test_muti_word_fit(self):
        str_list = split_text("Hello There", 11)
        self.assertEqual(str_list, ['Hello There'])

    def test_muti_word_no_fit(self):
        str_list = split_text("Hello There Monkey!", 6)
        self.assertEqual(str_list, ['Hello', 'There', 'Monkey!'])


if __name__ == '__main__':
    unittest.main()

""" Control the LCD display including controls for scrolling text """
# pylint: disable=C0326
import threading
from enum import Enum
from lcd.vert_split import split_text

class Scroll_mode(Enum):
    """ Define an enum that can be used with the set_text method """
    set_text = 0 # Text doesn't scroll
    write_text = 1 # Text written to screen with screen clearing. Use for direct access to display
    horizontal = 2 # Text scrolls horizontally
    vertical = 3 # Text scrolls vertically
    disabled = 4 # Thread doesn't do anything

class LCD_display(object):
    _BACKLIGHT_ON            = bytearray([0xFE, 0x42])
    _BACKLIGHT_OFF           = bytearray([0xFE, 0x46])
    _SET_BRIGHTNESS          = bytearray([0xFE, 0x99])
    _SET_CONTRAST            = bytearray([0xFE, 0x50])
    _AUTO_SCROLL_ON          = bytearray([0xFE, 0x51])
    _AUTO_SCROLL_OFF         = bytearray([0xFE, 0x52])
    _CLEAR_SCREEN            = bytearray([0xFE, 0x58])
    _SET_SPLASH_SCREEN       = bytearray([0xFE, 0x40])
    _SET_CURSOR_POS          = bytearray([0xFE, 0x47])
    _GO_HOME                 = bytearray([0xFE, 0x48])
    _CURSOR_BACK             = bytearray([0xFE, 0x4C])
    _CURSOR_FORWARD          = bytearray([0xFE, 0x4D])
    _UNDERLINE_CURSOR_ON     = bytearray([0xFE, 0x4A])
    _UNDERLINE_CURSOR_OFF    = bytearray([0xFE, 0x4B])
    _BLOCK_CURSOR_ON         = bytearray([0xFE, 0x53])
    _BLOCK_CURSOR_OFF        = bytearray([0xFE, 0x54])
    _SET_RGB_BACKLIGHT_COLOR = bytearray([0xFE, 0xD0])
    _SET_LCD_SIZE            = bytearray([0xFE, 0xD1])

    _LCD_COLS               = 16
    _LCD_ROWS               = 2

    # Setup color tuples for setting the background light.
    GREEN  = (0,   255, 0)
    YELLOW = (255, 100, 0)
    RED    = (255, 0,   0)
    ORANGE = (255, 20,  0)
    BLUE   = (11,  11,  255)
    #BLACK  = (0,   0,   0)
    BLACK  = (255, 255, 255)
    WHITE  = (255, 255, 255)


    def __init__(self, serial_file):
        self._serial_file = serial_file

        # Create a lock for access to write to the LCD
        self._write_lock = threading.RLock()

        # Create a lock for text access update
        self._text_lock = threading.RLock()

        # Setup some good defaults
        self.clear_screen()
        self.set_background_color_tuple(LCD_display.WHITE)
        self.set_contrast(190)
        self.set_brightness(100)
        self.set_block_cursor_enable(False)


        # Create an event to signal new text
        self._text_event = threading.Event()

        # Create an event to signal text has been written
        self._text_written_event = threading.Event()

        self._text = ""
        self._text_thread_enabled = True
        self._scroll_mode = Scroll_mode.disabled

        # Create a thread for displaying and scrolling text
        self._text_thread = threading.Thread(target = self._text_thread_func)
        self._text_thread.start()

    def shutdown(self):
        try:
            self._text_thread_enabled = False
            self._text_event.set() # Set the event to exit any sleeps
            self._text_thread.join()
            self._serial_file.close()
        except:
            pass


    def _lock_and_write(self, byte_list):
        try:
            # No need to write null terminating character.
            with self._write_lock:
                for bytes in byte_list:
                    self._serial_file.write(bytes)
                    self._serial_file.flush()
        except IOError:
            return False

        return True

    def _text_thread_func(self):
        """ This thread is responsible for updating the text on the display. It
        can scroll horizontally, vertically or not at all."""
        self.set_auto_scroll_enable(False) # Disable auto-scroll
        # Ensure that we run the loops to avoid spinning
        while self._text_thread_enabled:
            # Clear any set event, so the event.wait() call will block until a
            # new string is provided
            self._text_event.clear()

            # Get a local copy of the text
            with self._text_lock:
                text = self._text
                scroll_mode = self._scroll_mode

            # Display text based on the mode
            if scroll_mode == Scroll_mode.set_text:
                self._no_scroll(text)
            elif scroll_mode == Scroll_mode.horizontal:
                # Delegate to horizontal scrolling method
                self._horizontal_scroll(text)
            elif scroll_mode == Scroll_mode.vertical:
                # Delegate to vertical scrolling method
                self._vertical_scroll(text)
            elif scroll_mode == Scroll_mode.write_text:
                # Write the text and notify that it has been completed
                # so that write_text can block until it is written
                self._lock_and_write([text.encode()])
                # Change the mode to just block
                with self._text_lock:
                    self._scroll_mode = Scroll_mode.disabled
                    self._text_written_event.set()
            else: # Scolling disabled. Just block until a new command comes in.
                self._text_event.wait(1)

    def _no_scroll(self, text):
        try:
            self.set_auto_scroll_enable(False) # Disable auto-scroll
            sleep_time = 10 # 10 second

            # Break up the string into sections that will fit in the display
            str_list = split_text(text, self._LCD_COLS)

            # Get a local copy of the text
            self.clear_screen() # Clear the screen

            # Will be at pos 1,1
            self._lock_and_write([str_list[0].encode()])
            # Move to first line
            self.set_cursor_pos(1,2)
            if len(str_list) > 1:
                self._lock_and_write([str_list[1].encode()])

            self._text_event.wait(sleep_time) # Wait for a new text event

        except IOError:
            print("Error in _horizontal_scroll")

    def _horizontal_scroll(self, text):
        try:
            self.set_auto_scroll_enable(False) # Disable auto-scroll
            sleep_time = .300 #300 milliseconds

            # The text should start off the screen and appear a character at a
            # time until the whole message has been displayed. An easy way to
            # do this is to pad the string with whitespace for each column and
            # then just keep shifting the string.
            # Pad _LCD_COLS -1 so that the first character is immediately
            # displayed.
            text = text.rjust(len(text) + LCD_display._LCD_COLS - 1)
            str_len = len(text)

            # Scroll the text from right to left
            for index in range(0, str_len):

                # The number of characters to display will be min of LCD_COLS
                # and the remainder of the string
                if (str_len - index) < LCD_display._LCD_COLS:
                    count = str_len - index
                else:
                    count = LCD_display._LCD_COLS

                # Grab the portion of the string to display
                line = text[index: index + count]

                self.go_home() #Start at top left or bottom left for scrolling.
                self.clear_screen()
                self._lock_and_write([text.encode()]) # Write the line to the display

            # Check if a new string has been provided.
                # Using an event rather than a sleep allows the thread to
                # return immediately if new text is provided
                if not self._text_thread_enabled or self._text_event.wait(sleep_time):
                    return
        except IOError:
            print("Error in _horizontal_scroll")

    def _vertical_scroll(self, text):
        sleep_time = 3 #
        # Break up the string into sections that will fit in the display
        str_list = split_text(text, self._LCD_COLS)


        # Add in empty entries to scroll off the display before starting again
        for i in range(0, self._LCD_ROWS):
            str_list.append("")

        last_str = ""
        i = 0
        while i < len(str_list):
            self.clear_screen()

            # Will be at pos 1,1
            self._lock_and_write([str_list[i].encode()])
            # Move to second line
            self.set_cursor_pos(1,2)
            if (i + 1) < len(str_list):
            #print ("Writing: '%s'" % write_text)
                self._lock_and_write([str_list[i+1].encode()])

            # Advance to display the next two strings
            i = i + 2

            # Check if a new string has been provided.  Using an event rather
            # than a sleep allows the thread to return immediately if new text
            # is provided
            if not self._text_thread_enabled or self._text_event.wait(sleep_time):
                return



    def _set_text(self, text, scroll_mode=Scroll_mode.set_text):
        """ Display a string with a scroll mode"""
        if not ((scroll_mode == Scroll_mode.set_text)
                or (scroll_mode == Scroll_mode.write_text)
                or (scroll_mode == Scroll_mode.horizontal)
                or (scroll_mode == Scroll_mode.vertical)):
            print("Invalid scroll_mode %d" % scroll_mode)
            return False

        with self._text_lock:
            # Check if anything has changed
            if not self._scroll_mode == scroll_mode or not self._text == text:
                # Update the current scroll mode
                self._scroll_mode = scroll_mode
                # Update the current text
                self._text = text
                # Notify the text thread that new text is available
                self._text_event.set()
        return True

    def set_text(self, text):
        """ Clear any existing text and set the desired text"""
        return self._set_text(text, Scroll_mode.set_text)

    def write_text(self, text):
        """ Disable scrolling and write a string to the display."""
        # Clear the event
        self._text_written_event.clear()

        # Set the text
        retval = self._set_text(text, Scroll_mode.write_text)

        # Wait for the thread to write the text
        self._text_written_event.wait()

        return retval

    def scroll_text_horizontal(self, text):
        """ Clear any existing text and horizontally scroll the provided text"""
        return self._set_text(text, Scroll_mode.horizontal)

    def scroll_text_vertical(self, text):
        """ Clear any existing text and vertically scroll the provided text"""
        return self._set_text(text, Scroll_mode.vertical)


    def set_backlight_enable(self, enable):
        """
        Enable or disable the display backlight
        @param [in] enable If true backlight is enabled.
        @return true if write to device was successful, false otherwise.
        """
        if enable is True:
            return self._lock_and_write([LCD_display._BACKLIGHT_ON])
        else:
            return self._lock_and_write([LCD_display._BACKLIGHT_OFF])

    def set_brightness(self, value):
        """
        Set the brightness. Will be saved in EEPROM. The brightness will be
        applied after the color is set.
        @param [in] value 0 to 150 for a noticeable change. 100 is a good
        default.
        @return true if write to device was successful, false otherwise.
        """
        try:
            return self._lock_and_write([LCD_display._SET_BRIGHTNESS, bytearray([value])])
        except (IOError, ValueError):
            return False

    def set_contrast(self, value):
        """
        Set the contrast. Will be saved in EEPROM.
        @param [in] value 0 to 200? 180-200 is best.
        @return true if write to device was successful, false otherwise.
        """
        try:
            return self._lock_and_write([LCD_display._SET_CONTRAST, bytearray([value])])
        except (IOError, ValueError):
            return False
        return True

    def set_auto_scroll_enable(self, enable):
        """
        Enable or disable auto scroll.  This will make it so when text is
        received and there's not more space on the display, the text will
        automatically 'scroll' so the second line becomes the first line, etc.
        and new text is always at the bottom of the display.
        @param [in] enable If true auto scroll is enabled.
        @return true if write to device was successful, false otherwise.
        """
        if enable:
            return self._lock_and_write([LCD_display._AUTO_SCROLL_ON])
        else:
            return self._lock_and_write([LCD_display._AUTO_SCROLL_OFF])

    def clear_screen(self):
        """
        Clear any text on the screen.
        @return true if write to device was successful, false otherwise.
        """
        return self._lock_and_write([LCD_display._CLEAR_SCREEN])

    def set_splash_screen(self, text):
        """
        Specify the text that will be displayed as the splash screen when power
        is applied.
        @param [in] value is 32 characters (16x2)
        @return true if write to device was successful, false otherwise.
        """
        return self._lock_and_write([LCD_display._SET_SPLASH_SCREEN, text])

    def set_cursor_pos(self, col, row):
        """
        Set the cursor position
        Column and row numbers are 1 based so top left is (1,1)
        @return true if write to device was successful, false otherwise.
        """
        try:
            return self._lock_and_write([LCD_display._SET_CURSOR_POS, bytearray([col, row])])
        except ValueError:
            return False

    def go_home(self):
        """
        Place the cursor at location (1,1)
        @return true if write to device was successful, false otherwise.
        """
        return self._lock_and_write([LCD_display._GO_HOME])

    def cursor_back(self):
        """
        Move the cursor back one space. If at location (1,1), it will 'wrap' to
        the last position.
        @return true if write to device was successful, false otherwise.
        """
        return self._lock_and_write([LCD_display._CURSOR_BACK])

    def cursor_forward(self):
        """
        Move the cursor forward one space. If at the last location, it will
        'wrap' to the (1,1) position.
        @return true if write to device was successful, false otherwise.
        """
        return self._lock_and_write([LCD_display._CURSOR_FORWARD])

    def set_underline_cursor_enable(self, enable):
        """
        Enable the underlined cursor.
        @param [in] enable If true, the cursor will be underlined.
        @return true if write to device was successful, false otherwise.
        """
        if enable:
            return self._lock_and_write([LCD_display._UNDERLINE_CURSOR_ON])
        else:
            return self._lock_and_write([LCD_display._UNDERLINE_CURSOR_OFF])

    def set_block_cursor_enable(self, enable):
        """
        Enable the block cursor.
        @param [in] enable If true, the blinking block cursor will be enabled.
        @return true if write to device was successful, false otherwise.
        """
        if enable:
            return self._lock_and_write([LCD_display._BLOCK_CURSOR_ON])
        else:
            return self._lock_and_write([LCD_display._BLOCK_CURSOR_OFF])

    def set_background_color_tuple(self, colors):
        """ Sets the background color by passing in an RGB tuple. """
        return self.set_background_color(colors[0], colors[1], colors[2])

    def set_background_color(self, red, green, blue):
        """
        Set the background color. This is saved to EEPROM.
        @param [in] red The value of the red component 0x00-0xFF
        @param [in] green The value of the green component 0x00-0xFF
        @param [in] blue The value of the blue component 0x00-0xFF
        @return true if write to device was successful, false otherwise.
        """
        try:
            return self._lock_and_write(
                [LCD_display._SET_RGB_BACKLIGHT_COLOR, bytearray([red, green, blue])]
                )
        except (IOError, ValueError):
            return False

    def set_lcd_size(self, rows, cols):
        """
        Set the size of the display. Setting will be saved to EEPROM. Power
        needs to be cycled to apply change.
        @param [in] rows The number of rows.
        @param [in] cols The number of columns.
        @return true if write to device was successful, false otherwise.
        """
        try:
            return self._lock_and_write([LCD_display._SET_LCD_SIZE,bytearray([rows, cols])])
        except ValueError:
            return False

from lights.rotate_sequence import Rotate_sequence
from buttons.buttons import Buttons
from lcd.lcd_display import LCD_display
import threading


class Story(object):

    def __init__(self, light_list, lcd):
        self._lcd = lcd

        # Create a rotating light sequence
        self._light_seq = Rotate_sequence(True, light_list)

        # Create an event to signal a button has been pushed
        self._button_event = threading.Event()

        # Is the app enabled
        self._enabled = False

    def get_name(self):
        """ Get the name for this application"""
        return "Story App"

    def setup(self):
        """ Setup the initial state of the app."""

        self._light_seq.setup()
        self._story_green = """A long time ago in a galaxy far far away lived a master Jedi
        named Levi. Along with his best friend sparky the dog, he had many
        great adventures."""

        self._story_yellow = """Once upon a time, there was a dog named sparky. He lost his owner in
         a very, very dark cave.  Then when his owner lost him, he saw very
         many creepy things. They were spiders, black cats, bears and bats.
         So, he climbed up the rocky stairs and once he got up there he had to
         fight a dragon. First, the dragon breathed fire on Sparky, but Sparky
         dodged it. Then Sparky bit him on the lip.  Then, it started bleeding.
         Then Sparky did his powerful bark that pushed the dragon so far away
         into the ocean that the dragon couldn't fly and he drowned because it
         was so deep. And then he got the coin and guess what?!, Sparky
         appeared by his owner. Then they were so happy together. They never
         ever ever lost each other again. The end."""

        self._story_red = """Once upon a time there was a baby crying every
        day.  But, one day, he pooped in his diaper and he also peed in his
        diaper. And then one day, he had a stinky, stinky, stinky, diaper.  And
        then he cried like a baby.  And then once the baby always cries, he
        always poops in his diaper. The End."""

        self._story_orange = """Once upon a time, there was a death star.  The
        emperor liked being in it, so he had to get very bad to shoot
        lightening out of his hands. But, one day, in Star Wars, Anakin threw
        him down in a pit in the death start.  And then they lived happily ever
        after.  Then end."""

        self._story_blue = """Once upon a time... a person named Alex lost his
        parents and his sister long ago, the day he was going to get married.
        Then, his adventure began. He called his friend, Frock, the general of
        the US army and blew up the station they were keeping his parents in.
        And then, there was a witch in a hut that said, she knew where his
        sister was. She an outlaw in Idaho. So, they went to save her.
        And they did. And then, they went to look for the black treasure. When they got
        there, there was tons of skeletons. When he opened the chest, all of them came to life.
        They tried to shoot him and slay him. He go away on a boat at shore. And then the Kraken hurried up behind them.
        Then, the ship cracked and split in two and they stayed afloat on a piece of the ship.
        After that, they returned home. Next, they went to look at the old lab they blew up years ago.
        They found out, they were constructing a serum that creates zombies. So, they tried to 
        destroy the serum. And, then, they went to a mansion for help. He found the person who was going to get married with. Her name
        was Maria and she had a scar on her eye from a tiger a long time ago..."""

        self._lcd.scroll_text_vertical(self._story_green)

    def teardown(self):
        pass

    def stop(self):
        # Give the button event to exit the loop early
        self._button_event.set()
        self._enabled = False

    def run(self):
        """ Run until stopped. The text will already be scrolling in its own thread"""
        self._lcd.set_text("Press button to choose a story.")
        self._button_event.clear()
        self._enabled = True

        # Wait for a button press. The event will stay set until the sequence completes or
        # the application is stopped.
        while self._enabled:
            if self._button_event.wait(1):
                if self._enabled:
                    self._light_seq.update(1)


    def button_pressed(self,channel):
        # Just change the background color when a button is pushed.
        color = None
        story = self._story_green
        if (channel == Buttons.GREEN): # Green
            color = LCD_display.GREEN
        elif (channel == Buttons.YELLOW): # Yellow
            color = LCD_display.YELLOW
            story = self._story_yellow
        elif (channel == Buttons.RED): # Red
            color = LCD_display.RED
            story = self._story_red
        elif (channel == Buttons.ORANGE): # Orange
            color = LCD_display.ORANGE
            story = self._story_orange
        elif (channel == Buttons.BLUE): # Blue
            color = LCD_display.BLUE
            story = self._story_blue
        elif (channel == Buttons.BLACK): # Black
            color = LCD_display.WHITE
            #color = BLACK

        if not color is None:
            # Notify that a button has been pushed
            self._button_event.set()
            self._lcd.set_background_color_tuple(color)
            self._lcd.scroll_text_vertical(story)


""" Application to Select a Random Light and then provide a series of questions."""
import threading
from lights.random_sequence import Random_sequence
from buttons.buttons import Buttons
from lcd.lcd_display import LCD_display

class Questions(object):
    """The Random Text Selction class"""

    def __init__(self, light_list, lcd, sound_player):
        self._lcd = lcd
        self._sound_player = sound_player
        self._light_seq = Random_sequence(light_list)
        self._question_num = -1 # Will be incremented to 0 on first push
        self._spinning = False # Are we currently spinning
        self._question_list = []

        self._question_list.append("Why would the Israelites fight Goliath? 1 Samuel 17:8-9")
        self._question_list.append("Why didn't any of the Israelites want to fight Goliath? 1 Samuel 17:4-7")
        self._question_list.append("Why was David at the battlefield? 1 Samuel 17:17-18")
        self._question_list.append("What is a Goliath in your life? How can you defeat it?")
        self._question_list.append("Why was David's oldest brother angry with him when he heard David asking about Goliath? 1 Samuel 17:26-28")
        self._question_list.append("Why did David want to fight when everyone else was afraid?")
        self._question_list.append("What is a Goliath in your life? How can you defeat it?")
        self._question_list.append("What did David tell King Saul to convince him that he could kill Goliath? 1 Samuel 17:34-37")
        self._question_list.append("Whom did David give credit to for deliverying him from the bear and lion? 1 Samuel 17:37")
        self._question_list.append("How did Goliath prepare for the battle? 1 Samuel 17:5-7")
        self._question_list.append("What is a Goliath in your life? How can you defeat it?")
        self._question_list.append("What weapons did David use? 1 Samuel 17:38-40")
        self._question_list.append("What did David have that Goliath did not? 1 Samuel 17:45-47")
        self._question_list.append("Who should we depend on when we face challenges?")
        self._question_list.append("What do we have to do to be worthy of help?")
        self._question_list.append("How did Goliath react when he saw David coming to fight him? Samuel 17:42-44")
        self._question_list.append("How did David kill Goliath even though he had armor? Samuel 17:45-50")

        # Create an event to signal a button has been pushed
        self._button_event = threading.Event()

        # Is the app enabled
        self._enabled = False

    def get_name(self):
        """ Get the name for this application"""
        return "Church Questions"

    def setup(self):
        """ Setup the initial state of the app."""
        print("Random Text setup")
        self._light_seq.setup()

    def teardown(self):
        pass

    def stop(self):
        # Give the button event to exit the loop early
        self._button_event.set()
        self._enabled = False

    def run(self):
        """ Run until stopped"""
        # Wait for a button press. The event will stay set until the sequence completes or
        # the application is stopped.
        self._lcd.set_text("Green, Yellow or Red to Spin")
        self._button_event.clear()
        self._enabled = True

        print("Question App Run")
        while self._enabled:
            if self._button_event.wait(1):
                if self._enabled:
                    retval = self._light_seq.update(.7)

                    # When the light sequence is finished and has chosen a number
                    # it will be returned and will be greater than 0.
                    if retval > 0:
                        self._spinning = False
                        text = "Press Orange For Question"

                        # We don't care which value was returned. The questions
                        # will be asked in order.
                        #if retval <= len(self._question_list):
                        #    text = self._question_list[retval-1]

                        #self._lcd.scroll_text_horizontal("%s" % (text))
                        #self._lcd.scroll_text_vertical("%s: %s" % (retval, text))
                        self._lcd.set_text("%s" % (text))

                        # Setup the light sequence to pick the next random light
                        self._light_seq.setup()
                        # If a random selection has been made yet, clear the event
                        # to wait for another button press
                        self._button_event.clear()

                        # Play a winning or losing sound
                        #if retval == 5:
                            #self._sound_player.play_file("audio/cheering.wav")
                        #else:
                            #self._sound_player.play_file("audio/sad_trombone_short.wav")
                    else:
                        self._spinning = True

                else:
                    # If disabled setup to wait for another button press
                    self._button_event.clear()


    def button_pressed(self, channel):
        color = None
        if channel == Buttons.GREEN:
            self._lcd.set_text("Spinning the wheel ...")
            color = LCD_display.GREEN
            # Notify that a button has been pushed
            self._button_event.set()
        elif channel == Buttons.YELLOW:
            self._lcd.set_text("Spinning the wheel ...")
            color = LCD_display.YELLOW
            # Notify that a button has been pushed
            self._button_event.set()
        elif channel == Buttons.RED:
            self._lcd.set_text("Spinning the wheel ...")
            color = LCD_display.RED
            # Notify that a button has been pushed
            self._button_event.set()
        elif channel == Buttons.ORANGE:
            # Only allow a question if the wheel isn't spinning.
            if (not self._spinning):
                # Go the the next question, wrapping if necessary
                self._question_num += 1
                if self._question_num >= len(self._question_list):
                    self._question_num = 0
                # Set the text to the question
                        #self._lcd.scroll_text_vertical("%s: %s" % (retval, text))
                self._lcd.scroll_text_vertical("%s: %s" % (self._question_num + 1, self._question_list[self._question_num]))
                color = LCD_display.ORANGE
        elif channel == Buttons.BLUE:
            # Only allow a question if the wheel isn't spinning.
            if (not self._spinning):
                # Go to previous question, wrapping if necessary
                self._question_num -= 1
                if self._question_num < 0:
                    self._question_num = len(self._question_list) - 1
                # Set the text to the question
                self._lcd.scroll_text_vertical("%s: %s" % (self._question_num + 1, self._question_list[self._question_num]))
                color = LCD_display.BLUE
        elif channel == Buttons.BLACK:
            self._lcd.set_text("Giant black spiders above!")
            color = LCD_display.BLACK

        if not color is None:
            self._lcd.set_background_color_tuple(color)


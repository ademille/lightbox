""" Application to Select a Random Text message"""
import threading
import time
from lights.prize_wheel_sequence import Prize_wheel_sequence
from buttons.buttons import Buttons
from lcd.lcd_display import LCD_display
from enum import Enum

class State(Enum):
    WAIT_FOR_START = 0
    FAST_SPIN = 1
    CHOICE_MADE = 2
    STOP_SPIN = 3

class Prize_wheel(object):
    """The Random Text Selction class"""

    def __init__(self, light_list, lcd, sound_player):
        self._lcd = lcd
        self._sound_player = sound_player
        self._prize_light_seq = Prize_wheel_sequence(light_list)

        # Create an event to signal a button has been pushed
        self._button_event = threading.Event()

        # Is the app enabled
        self._enabled = False

        self._state = State.WAIT_FOR_START
        self._selected = -1

    def get_name(self):
        """ Get the name for this application"""
        return "Halloween"

    def setup(self):
        """ Setup the initial state of the app."""
        print("Prize Wheel Setup")
        self._prize_light_seq.setup()

    def teardown(self):
        pass

    def stop(self):
        # Give the button event to exit the loop early
        self._button_event.set()
        self._enabled = False

    def run(self):
        """ Run until stopped"""
        # Wait for a button press. The event will stay set until the sequence completes or
        # the application is stopped.
        self._lcd.set_text("Press button to start.")
        self._button_event.clear()
        self._enabled = True

        print("Prize wheel run")
        speed = 2
        while self._enabled:
            if self._state is State.WAIT_FOR_START:
                time.sleep(.2)
            elif self._state is State.FAST_SPIN:
                self._prize_light_seq.update(speed)

            elif self._state is State.CHOICE_MADE:
                #self._prize_light_seq.choose_current()
                #self._state = State.STOP_SPIN
                #self._state = State.FAST_SPIN
                pass

            elif self._state is State.STOP_SPIN:
                #retval = self._prize_light_seq.update(speed)
                retval = self._selected
                if retval > 0:
                    # Play a winning or losing sound
                    print("Final light is: %d" % (retval))
                    if retval == 5 or retval == 1:
                        self._sound_player.play_file("audio/cheering.wav")
                    elif retval == 3 or retval == 7:
                        self._sound_player.play_file("audio/sad_trombone_short.wav")
                    else:
                        self._sound_player.play_file("audio/game-success-notification-short.wav")
                    # Setup for next play
                    self._prize_light_seq.setup()
                    self._selected = -1
                    self._state = State.WAIT_FOR_START

                    # If a random selection has been made yet, clear the event
                    # to wait for another button press
                    self._button_event.clear()




    def button_pressed(self, channel):
        color = None
        if channel == Buttons.GREEN:
            color = LCD_display.GREEN
        elif channel == Buttons.YELLOW:
            color = LCD_display.YELLOW
        elif channel == Buttons.RED:
            color = LCD_display.RED
        elif channel == Buttons.ORANGE:
            color = LCD_display.ORANGE
        elif channel == Buttons.BLUE:
            color = LCD_display.BLUE
        elif channel == Buttons.BLACK:
            color = LCD_display.BLACK

        print("Button pressed. State = %s" % (self._state.name))
        if self._state == State.WAIT_FOR_START:
            print("Setting state to FAST_SPIN")
            self._state = State.FAST_SPIN
        elif self._state == State.FAST_SPIN:
            print("Setting state to CHOICE_MADE")
            #self._state = State.CHOICE_MADE
            self._selected = self._prize_light_seq.stop_now()
            self._state = State.STOP_SPIN

        print("Button pressed. New State = %s" % (self._state.name))
        

        if not color is None:
            self._button_event.set()
            self._lcd.set_background_color_tuple(color)

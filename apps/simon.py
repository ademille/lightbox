""" Simon Says Type Game"""
import threading
from lights.rotate_sequence import Rotate_sequence
from buttons.buttons import Buttons
from lcd.lcd_display import LCD_display


class Simon(object):

    def __init__(self, light_list, lcd):
        self._light_list = light_list
        self._lcd = lcd

        # Create a rotating light sequence
        self._light_seq = Rotate_sequence(True)

        # Create an event to signal a button has been pushed
        self._button_event = threading.Event()

        # Is the app enabled
        self._enabled = False

    def get_name(self):
        """ Get the name for this application"""
        return "Simon Says"

    def setup(self):
        """ Setup the initial state of the app."""

        #self._light_seq.setup(self._light_list)

    def teardown(self):
        pass

    def stop(self):
        # Give the button event to exit the loop early
        self._button_event.set()
        self._enabled = False

    def step(self):
        """ Rotate the lights. The text will already be scrolling in its own thread"""
        # Wait for a button press. The event will stay set until the sequence completes or
        # the application is stopped.
        if self._button_event.wait(1):
            if self._enabled:
                self._light_seq.update(1)

        return self._enabled

    def button_pressed(self,channel):
        # Just change the background color when a button is pushed.
        color = None
        if channel == Buttons.GREEN: # Green
            color = LCD_display.GREEN
        elif channel == Buttons.YELLOW: # Yellow
            color = LCD_display.YELLOW
        elif channel == Buttons.RED: # Red
            color = LCD_display.RED
        elif channel == Buttons.ORANGE: # Orange
            color = LCD_display.ORANGE
        elif channel == Buttons.BLUE: # Blue
            color = LCD_display.BLUE
        elif channel == Buttons.BLACK: # Black
            color = LCD_display.WHITE

        if not color is None:
            # Notify that a button has been pushed
            self._button_event.set()
            self._lcd.set_background_color_tuple(color)


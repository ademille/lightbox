from buttons.buttons import Buttons
from lcd.lcd_display import LCD_display
import time
import threading
import random
import socket


class Menu(object):

    def __init__(self, light_list, lcd):
        self._app_list = []
        self._lcd = lcd
        # Create an event to signal a button has been pushed
        self._app_selected_event = threading.Event()
        self._app_index = 0
        self._accept_text = "Green to select."
        self._enabled = False

    def _get_IP(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        try:
            # doesn't even have to be reachable
            s.connect(('10.255.255.255', 1))
            IP = s.getsockname()[0]
        except:
            IP = '127.0.0.1'
        finally:
            s.close()
        return IP

    def is_enabled(self):
        return self._enabled

    def add_app(self, app):
        # Add the app to the list
        self._app_list.append(app)

    def get_selected_app(self):
        self._enabled = True
        self._app_selected_event.clear()
        self._lcd.set_background_color_tuple(LCD_display.WHITE)
        # Display the current app, with the IP address of the pi
        IP = self._get_IP()
        if self._app_index < len(self._app_list):
            self._lcd.scroll_text_horizontal("#" + self._app_list[self._app_index].get_name() + "# " + self._accept_text + " IP: " + IP)
        else:
            self._lcd.scroll_text_horizontal("No valid applications")

        # Wait for the app to be selected. It will be selected based on button
        # pushes
        while not self._app_selected_event.wait(1):
            pass

        self._enabled = False
        return self._app_list[self._app_index]


    def button_pressed(self,channel):
        # Just change the background color when a button is pushed.
        if (channel == Buttons.GREEN): # Green
            # Select current app
            self._app_selected_event.set()
        elif (channel == Buttons.BLACK): # Black
            #Select next application
            self._app_index = self._app_index + 1
            if self._app_index >= len(self._app_list):
                self._app_index = 0
            # Display the name of the selected app
            self._lcd.scroll_text_horizontal("#" + self._app_list[self._app_index].get_name() + "# " + self._accept_text)
        else: # Invalid menu button
            pass


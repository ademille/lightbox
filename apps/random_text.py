""" Application to Select a Random Text message"""
import threading
from lights.random_sequence import Random_sequence
from buttons.buttons import Buttons
from lcd.lcd_display import LCD_display
from enum import Enum

class State(Enum):
    NORMAL_SPIN = 0
    CONTINUOUS_SPIN = 1

class Random_text(object):
    """The Random Text Selction class"""

    def __init__(self, light_list, lcd, sound_player):
        self._lcd = lcd
        self._sound_player = sound_player
        self._light_seq = Random_sequence(light_list)
        self._text_list = []
        #self._text_list.append("Act like your favorite animal.")
        #self._text_list.append("Pat your head and rub your tummy.")
        #self._text_list.append("Act like a dinosaur.")
        #self._text_list.append("Do 10 jumping jacks.")
        #self._text_list.append("Give your mom a hug.")
        #self._text_list.append("Give your dad a high-five.")
        #self._text_list.append("Run upstairs and back down as fast as you can.")
        #self._text_list.append("Jump as high as you can!")

        #self._text_list.append("Scout Oath: On my honor I will do my best To do my duty to God and my country and to obey the Scout Law; To help other people at all times; To keep myself physically strong, mentally awake, and morally straight. Page 18.")
        #self._text_list.append("Scout Law: A Scout is trustworth, loyal, helpful, friendly, courteous, kind, obedient, cheerful, thrifty, brave, clean, and reverent. Page 20.")
        #self._text_list.append("Show the cub scout sign. Tell what it means. Page 22.")
        #self._text_list.append("Show the cub scout hand shake. Tell what it means. Page 23.")
        #self._text_list.append("Say the cub scout motto. Tell what it means. Page 24.")
        #self._text_list.append("Show the cub scout salute. Tell what it means. Page 25.")
        #self._text_list.append("Act like a monkey.")
        #self._text_list.append("Give your family a hug.")

        self._text_list.append("Happy Halloween!")
        self._text_list.append("Sorry. You\ndidn't win.")
        self._text_list.append("Better luck\nnext time.")
        self._text_list.append("Thanks for\nplaying.")
        self._text_list.append("You Won a\nspecial treat!!")
        self._text_list.append("Great costume!")
        self._text_list.append("Watch out for\nmonsters!")
        self._text_list.append("Trick or Treat")

        # Create an event to signal a button has been pushed
        self._button_event = threading.Event()

        # Is the app enabled
        self._enabled = False

        # Does the user decide when to stop the spin
        self._user_stop_spin = False


    def get_name(self):
        """ Get the name for this application"""
        #return "Cub Scout App"
        return "Halloween"

    def setup(self):
        """ Setup the initial state of the app."""
        print("Random Text setup")
        self._light_seq.setup()

    def teardown(self):
        pass

    def stop(self):
        # Give the button event to exit the loop early
        self._button_event.set()
        self._enabled = False

    def run(self):
        """ Run until stopped"""
        # Wait for a button press. The event will stay set until the sequence completes or
        # the application is stopped.
        self._lcd.set_text("Press button to start.")
        self._button_event.clear()
        self._enabled = True

        print("Random Text run")
        while self._enabled:
            if self._button_event.wait(1):
                if self._enabled:
                    retval = self._light_seq.update(.6)

                    if retval > 0:
                        text = "What's up?"
                        if retval <= len(self._text_list):
                            text = self._text_list[retval-1]

                        #self._lcd.scroll_text_horizontal("%s: %s" % (retval, text))
                        #self._lcd.scroll_text_vertical("%s: %s" % (retval, text))
                        self._lcd.set_text("%s" % (text))

                        # Setup the light sequence to pick the next random light
                        self._light_seq.setup()
                        # If a random selection has been made yet, clear the event
                        # to wait for another button press
                        self._button_event.clear()

                        # Play a winning or losing sound
                        if retval == 5:
                            self._sound_player.play_file("audio/cheering.wav")
                        else:
                            self._sound_player.play_file("audio/sad_trombone_short.wav")

                else:
                    # If disabled setup to wait for another button press
                    self._button_event.clear()


    def button_pressed(self, channel):
        color = None
        if channel == Buttons.GREEN:
            self._lcd.set_text("Green slimy ghosts! Oh no!")
            color = LCD_display.GREEN
        elif channel == Buttons.YELLOW:
            self._lcd.set_text("Yellow skinned zombies! Run!")
            color = LCD_display.YELLOW
        elif channel == Buttons.RED:
            self._lcd.set_text("Red eyed monsters! Yikes!")
            color = LCD_display.RED
        elif channel == Buttons.ORANGE:
            self._lcd.set_text("Orange you glad it's halloween?")
            color = LCD_display.ORANGE
        elif channel == Buttons.BLUE:
            self._lcd.set_text("Why so blue? You might win!")
            color = LCD_display.BLUE
        elif channel == Buttons.BLACK:
            self._lcd.set_text("Press again to stop the spin!")
            color = LCD_display.BLACK

        if not color is None:
            # Notify that a button has been pushed
            # If the black button is pushed, then let the user stop the spin
            self._user_stop_spin =  color is LCD_display.BLACK
            self._button_event.set()
            self._lcd.set_background_color_tuple(color)


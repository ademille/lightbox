"""The main entry point for the Lightbox application"""
# pylint: disable=C0103

from lights.light_list import Light_list
from buttons.buttons import Buttons
from lcd.lcd_display import LCD_display
from lcd.simulated_lcd_device import Simulated_LCD_device
from apps.random_text import Random_text
from apps.questions import Questions
from apps.story import Story
from apps.prize_wheel import Prize_wheel
from apps.menu import Menu
from audio.sound_player import Sound_player
import time

on_rpi = True
try:
    import RPi.GPIO as GPIO
except ImportError:
    on_rpi = False

def button_pressed(button):
    global current_app
    global single_app

    # If in menu just delegate to menu application
    if menu.is_enabled():
        menu.button_pressed(button)
    else:
        # Delegate button press to active application, unless just a single
        # application is running
        if button == Buttons.BLACK and single_app is False:
            if current_app is not None:
                # Ask the active app to stop.
                current_app.stop()
        else:
            if current_app is not None:
                current_app.button_pressed(button)

if on_rpi:
    # Use broadcom mode for GPIOs
    GPIO.setmode(GPIO.BCM)

# Create a sound player object
sound_player = Sound_player()

# Setup LCD driver
# Open the serial file for binary output
if on_rpi:
    serial_file = '/dev/ttyACM0'
    try:
        serial_dev = open(serial_file, 'wb')
    except IOError:
        print("Could not open file: %s" % serial_file)
else:
    serial_dev = Simulated_LCD_device()

lcd = LCD_display(serial_dev)

lcd.scroll_text_horizontal("Welcome to Dad's lightbox! Press Black button for menu.")


# Setup access to the lights
light_list = Light_list().get_list()

# Create an app to choose a random number
random_text_app = Random_text(light_list, lcd, sound_player)

# Create an app to choose a person and then ask a question
questions_app = Questions(light_list, lcd, sound_player)

# Create an app to tell a story
story_app = Story(light_list, lcd)

# Create an app to tell a story
prize_wheel_app = Prize_wheel(light_list, lcd, sound_player)

# Create a menu to select the application to run
menu = Menu(light_list, lcd)

# Create a button object to detect button presses
buttons = Buttons(button_pressed)

# Keep track of the currently running app, so it will only be initialized if necessary
current_app = None

# Add the applications to the menu
menu.add_app(questions_app)
menu.add_app(random_text_app)
menu.add_app(story_app)
menu.add_app(prize_wheel_app)

# Add ability to disable menu and just have a single application
single_app = True

if single_app is True:
    app = prize_wheel_app
    #app = random_text_app

try:
    while True:
        if not single_app:
            # Wait for an application to be selected
            print("Waiting for an app to be selected")
            app = menu.get_selected_app()
            print("App %s was selected" % app.get_name())

        # An application has been selected from the menu
        if app is not current_app:
            if current_app is not None:
                # Tear down the current app
                current_app.teardown()

            # Setup the new app
            app.setup()

            # Update the current app
            current_app = app

        # Since we have just come from a menu, run the app
        # It will block until the app exits.
        app.run()

        print ("The application has exited")

except KeyboardInterrupt:
    print("CTRL+C detected")

lcd.shutdown() # Stop LCD threads
if on_rpi:
    GPIO.cleanup() # clean  up GPIO on normal exit

import pygame
import time

class Sound_player(object):

    def __init__(self):
        self.sound_channel = None


    def play_file(self, sound_file):
        # Make sure the mixer is initialized
        if (pygame.mixer.get_init() is None):
            pygame.mixer.init(48000, -16, 1, 8192)
            #pygame.mixer.init()

        # Check if a sound is already playing
        if self.sound_channel is None or not self.sound_channel.get_busy():
            sound = pygame.mixer.Sound(sound_file)
            self.sound_channel = sound.play()
            while self.sound_channel.get_busy() == True:
                time.sleep(.01)
                continue

on_rpi = True
try:
    import RPi.GPIO as GPIO
except ImportError:
    import readchar
    on_rpi = False


class Buttons(object):

    # Define constants that can be used for the button press callback
    GREEN  = 5
    YELLOW = 6
    RED    = 13
    ORANGE = 19
    BLUE   = 20
    BLACK  = 21

    def __init__(self, button_pressed_callback):
        if on_rpi:
            # Setup GPIOs for the Buttons
            button_GPIO_list = [Buttons.GREEN, Buttons.YELLOW,
                                Buttons.RED, Buttons.ORANGE, Buttons.BLUE, Buttons.BLACK]
            print("Setting up GPIOs for button access.")
            for pin in button_GPIO_list:
                GPIO.setup(pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
                # Setup callback for GPIO
                GPIO.add_event_detect(
                    pin, GPIO.RISING, callback=button_pressed_callback, bouncetime=200)
        else:
            # Start thread to monitor keypresses using 
            pass
